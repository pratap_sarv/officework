
const mongoose = require("mongoose");
var studentmodel = require("../Models/StudentSchema");
const MONGO_URI = 'mongodb+srv://guru:guru123456@cluster0.jsvae.mongodb.net/test?retryWrites=true&w=majority'

let db;
mongoose.connect(MONGO_URI, {
    useNewUrlParser: true,
    // useFindAndModify: false,
    useUnifiedTopology: true

},
    (err, cb) => {
        if (err) return console.log("Unable to connect with data base Error:", err);
        console.log("Successfully connected with DB!")
        db = cb
    });

module.exports = db;