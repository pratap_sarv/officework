const express = require("express");
const conn=require('./elasticconn');
// const res = require("express/lib/response");
const router=express.Router()
router.get('/data',(req,res)=>{
    let query={
        index:'value',
        id: "test"
    }
    conn.get(query)
    .then(resp=>{
        if(!resp){
            return res.status(404).json({
                product:resp
            });
        }
        return res.status(202).json({
            product:resp
        });
    })
    .catch(err=>{
        return res.status(500).json({
            msg:'error not found',
        });
    });

});
module.exports=router;
