const express = require('express');
const app = express();
const router = express.Router();
const Joi = require('joi');
// Joi.objectId = require('joi-objectid')(Joi);

const async = require('async');
const bodyParser = require('body-parser')
const db = require('./Config/Databaseconn')


var imgModel = require('./Models/ImageSchema');

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

app.set("view engine", "ejs");


const PORT = 3000;


       
const route = require("./Router/apiRouter");
app.use('/', route)


app.listen(PORT, () => {
  console.log(`Server running at: http://localhost:${PORT}/`);
});


