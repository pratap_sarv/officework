const imgModel = require('../Models/ImageSchema')
const fs=require('fs');
// let data=fs.readFileSync
const path=require('path')

// const test = require('../upload')
module.exports = async function uploadimage  (req, res, next)  {
 let newpath=path.resolve(__dirname, '../upload/');
    var obj = {
        name: req.body.name,
        desc: req.body.desc,
        img: {
           data: fs.readFileSync(path.join(newpath + '/' + req.file.filename)),

            contentType: 'image/png'
        }
    }
    imgModel.create(obj, (err, item) => {
        if (err) {
            console.log(err);
        }
        else {
            // item.save();
           return res.status(200).json({message:"success"})
        }
    });
}