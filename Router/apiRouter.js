var express = require('express')
var route = express.Router();
const multer = require('multer')
const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, 'upload');
    },
    filename: (req, file, cb) => {
        const fileName = file.originalname.toLowerCase().split(' ').join('-');
        cb(null, fileName)
    }
});

const upload = multer({
    storage: storage,
    fileFilter: (req, file, cb) => {
        if (file.mimetype == "image/png" || file.mimetype == "image/jpg" || file.mimetype == "image/jpeg" || file.mimetype == "image/gif") {
            cb(null, true);
        } else {
            cb(null, false);
            return cb(new Error('Allowed only .png, .jpg, .jpeg and .gif'));
        }
    }
});
var insertdata= require('../Controller/InsertData')
var Getdata = require('../Controller/GetData')
var Getimage = require('../Controller/UploadImage')
var userregs= require('../Controller/UserRegsInfo')
var user = require('..')
route.get('/getdata', Getdata);
route.post('/insertdata', insertdata);
route.post('/image', upload.single('image'), Getimage);
 route.post('/user',userregs);

// const {UserRegs, validate}= require('../Models/UserRegs')

// route.post('/', async (req, res) => {
//     // First Validate The Request
//     const { error } = validate(req.body);
//     if (error) {
//         return res.status(400).send(error.details[0].message);
//     }

//     // Check if this user already exisits
//     let user = await User.findOne({ email: req.body.email });
//     if (user) {
//         return res.status(400).send('That user already exisits!');
//     } else {
//         // Insert the new user if they do not exist yet
//         user = new User({
//             UserId: req.body.UserId,
//             UserName: req.body.UserName,
//             Email: req.body.Email,
//             Password: req.body.Password
//         });
//         await user.save();
//         res.send(user);
//     }
// });

 //module.exports = router;

module.exports = route;